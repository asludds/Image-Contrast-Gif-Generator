from PIL import Image, ImageEnhance
import imageio
import numpy as np
def main():
    #import image
    image = Image.open('./tree.png')
    image = image.convert('LA')
    #change image convert
    filenames = []
    rangeOfContrasts = np.linspace(1.0,5.0,50)
    for x in rangeOfContrasts:
        contrast = ImageEnhance.Contrast(image)
        imagecontrast = contrast.enhance(x)
        imagecontrast.save('./exports/imagecontrast' + str(x) + '.png')
        filenames.append('./exports/imagecontrast' + str(x) + '.png')
    images = []
    for filename in filenames:
        images.append(imageio.imread(filename))
    imageio.mimsave('./exports/main.gif',images)
    


if __name__ == "__main__":
    main()
